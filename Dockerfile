FROM ubuntu:focal
MAINTAINER Thomas Bella <thomas+docker@bella.network>

# Install packages
RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get upgrade -y && DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends -y ca-certificates git unzip wget curl python3 python3-pip openssh-client rsync iputils-ping coreutils jq && apt-get clean && apt-get autoclean && rm -rf /var/lib/apt/lists/*

# Install platformio
RUN pip3 install -U platformio && platformio update && platformio upgrade && platformio update

VOLUME ["/app"]
WORKDIR /app

CMD ["/bin/sh"]
